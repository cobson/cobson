# COBSON protocol

[![pipeline status](https://gitlab.com/vborshch/cobson/badges/master/pipeline.svg)](https://gitlab.com/vborshch/cobson/-/commits/master)

This is a simple DAQ protocol. Basically, it's developing for the UART-based, byte-width interfaces. The base of protocol is [COBS](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing).

The test environment is bulded with cocotb.

## Dependencies

- Python 3.8
- Icarus Verilog 12.0 *or* ModelSim/QuestaSim
- cocotb 1.6.1
- SystemVerilog IEEE 1800-2012

> Feel free to use any simulator with SV support

## Usage

    $ git clone https://gitlab.com/vborshch/cobson --recursive

## API

Python and C/C++ libraries

## Parameters list

TODO
