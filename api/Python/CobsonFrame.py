
from crc import CrcCalculator, Crc8
crc_calculator = CrcCalculator(Crc8.CCITT)

class CobsonFrame():
    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def __init__(self, data=b'', addr=None, cmd=None, tx_complete=None):
        self.addr = None
        self.cmd = None
        self.size = None
        self.data = bytearray()
        self.crc = None

        self.sim_time_start = None
        self.sim_time_end = None
        self.tx_complete = None

        if type(data) is CobsonFrame:
            if data.addr is not None:
                self.addr = data.addr
            else:
                print("Empty address")

            if data.cmd is not None:
                self.cmd = data.cmd
            else:
                print("Empty command")

            if type(data.data) is bytearray:
                self.data = bytearray(data.data)
            else:
                self.data = list(data.data)

            if data.size is not None:
                self.size = bytearray(data.size)
            else:
                self.size = len(data.data)

            self.sim_time_start = data.sim_time_start
            self.sim_time_end = data.sim_time_end
            self.tx_complete = data.tx_complete
        elif type(data) in (bytes, bytearray):
            self.data = bytearray(data)
            self.size = len(self.data)
        else:
            self.data = list(data)
            self.size = len(self.data)

        if tx_complete is not None:
            self.tx_complete = tx_complete

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def handle_tx_complete(self):
        if isinstance(self.tx_complete, Event):
            self.tx_complete.set(self)
        elif callable(self.tx_complete):
            self.tx_complete(self)

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def __eq__(self, other):
        if not isinstance(other, CobsonFrame):
            return False

        if self.data != other.data:
            return False

        if self.addr is not None and other.addr is not None:
            if self.addr != other.addr:
                return False

        if self.cmd is not None and other.cmd is not None:
            if self.cmd != other.cmd:
                return False

        return True

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def __repr__(self):
        return (
            f"{type(self).__name__}(data={self.data!r}, "
            f"addr={self.addr!r}, "
            f"cmd={self.cmd!r}, "
            f"crc={self.crc!r}, "
            f"sim_time_start={self.sim_time_start!r}, "
            f"sim_time_end={self.sim_time_end!r})"
        )

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def __len__(self):
        return len(self.addr)+len(self.cmd)+len(self.data)+len(self.crc)

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def __bytes__(self):
        return bytes([self.addr, self.cmd, self.data, self.crc])

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def cobs_encode(self, block):
        block = bytearray(block)
        enc = bytearray()

        seg = bytearray()
        code = 1

        new_data = True

        for b in block:
            if b == 0:
                enc.append(code)
                enc.extend(seg)
                code = 1
                seg = bytearray()
                new_data = True
            else:
                code += 1
                seg.append(b)
                new_data = True
                if code == 255:
                    enc.append(code)
                    enc.extend(seg)
                    code = 1
                    seg = bytearray()
                    new_data = False

        if new_data:
            enc.append(code)
            enc.extend(seg)

        return bytes(enc)


    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def cobs_decode(self, block):
        block = bytearray(block)

        if 0 in block:
            return None

        code = 0
        i = 0
        dec = bytearray()
        while i < len(block):
            code = block[i]
            i += 1
            if i+code-1 > len(block):
                return None
            dec.extend(block[i:i+code-1])
            i += code-1
            if code < 255 and i < len(block):
                dec.append(0)

        return bytes(dec)

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def cobson_encode(self, block, addr, cmd):
        block = bytearray(block)

        if len(block) < 1:
            return None

        enc = bytearray()
        enc.extend(bytes(addr))
        enc.extend(bytes(cmd))
        enc.extend(bytes([len(block)]))
        enc.extend(block)
        enc.extend(bytes([crc_calculator.calculate_checksum(enc)]))

        return enc

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def cobson_decode(self, block):
        block = bytearray(block)

        if len(block) < 4:
            return None

        dec = {'addr':0, 'cmd':0, 'size':0, 'data':bytearray(), 'crc':0}
        i = 0
        bunch_size = 0
        while i < len(block):
            if i == 0:
                dec['addr'] = bytes([block[i]])
            elif i == 1:
                dec['cmd'] = bytes([block[i]])
            elif i == 2:
                dec['size'] = bytes([block[i]])
                bunch_size = block[i]
            elif i > 2:
                if i == (3+bunch_size):
                    dec['crc'] = bytes([block[i]])
                else:
                    dec['data'].extend(bytes([block[i]]))
            i += 1

        return dec

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def get_bytes(self):
        encoded = self.cobson_encode(self.data, self.addr, self.cmd)
        self.crc = bytes([encoded[-1]])
        encoded = self.cobs_encode(encoded)
        return bytearray(encoded)

    #----------------------------------------------------------------------------------
    #
    #----------------------------------------------------------------------------------
    def set_bytes(self, block):
        decoded = self.cobs_decode(block)
        decoded = self.cobson_decode(decoded)
        self.addr = decoded['addr']
        self.cmd = decoded['cmd']
        self.size = decoded['size']
        self.data = decoded['data']
        self.crc = decoded['crc']
