
import itertools
import serial
import time
import sys

from CobsonFrame import CobsonFrame

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def main(args):
    try:
        # Prepare COM port
        comport = serial.Serial('COM4')
        comport.baudrate = int(1e6)
        comport.timeout  = 0.5
        comport.parity   = serial.PARITY_NONE
        comport.stopbits = int(2)
        comport.bytesize = serial.EIGHTBITS
        
        if comport.isOpen():
            print(comport)
        else:
            print("COM opening port error")

        # Init test frame
        rx_frame = CobsonFrame()

        tx_frame = CobsonFrame()
        tx_frame.addr = bytes([0])
        tx_frame.cmd = bytes([253])
        tx_frame.data = bytearray()
        
        start_time = time.time()
        for i in range(1, 10):
            test_data = bytearray(incrementing_payload(i))
            tx_frame.data = test_data
            raw_bytes = tx_frame.get_bytes()
            raw_bytes.extend(b'\x00')

            comport.write(raw_bytes)

            rx_array = bytearray()
            data = comport.read()
            while data != b'\x00':
                rx_array.extend(data)
                
                data = comport.read()

                if (data == b''):
                    break

            print(rx_array)

            if (len(data) > 0):
                rx_frame.set_bytes(rx_array)

                print("TX:", tx_frame)
                print("RX:", rx_frame)

                # assert tx_frame == rx_frame

                # if (cnt_frame % 1000 == 0):
                #     print("Tested", cnt_frame, "out of", n_iterations)

            else:
                print("Unsuccessful reading")

            # print(test_data)

        elapsed_time = float(time.time() - start_time)
        # print("Average FPS is:", (cnt_frame/elapsed_time))
    except Exception as e:
        print("Error", e)

if __name__ == '__main__':
    main(sys.argv)