#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, Timer
from cocotb.regression import TestFactory

from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink
from cocotbext.uart import UartSink, UartSource

import sys
sys.path.append('./../../api/Python')
from CobsonFrame import CobsonFrame


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut, baud=1e6):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 8, units="ns").start())

        self.axi_source = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.axi_sink = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)

        self.uart_sink = UartSink(dut.ouart_tx, baud=baud, bits=8, stop_bits=1)
        self.uart_source = UartSource(dut.iuart_rx, baud=baud, bits=8, stop_bits=1)

        dut.idivider.setimmediatevalue(int(1e9/baud/8))

    def set_idle_generator(self, generator=None):
        if generator:
            self.axi_source.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.axi_sink.set_pause_generator(generator())

    async def reset(self):
        self.dut.irst.setimmediatevalue(0)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 1
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_tx(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)
    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_data in [payload_data(x) for x in payload_lengths()]:
        encoded_data = CobsonFrame.cobs_encode(0, test_data)+b'\00'
        await tb.axi_source.write(test_data)

        rx_data = bytearray()
        while len(rx_data) < int(len(encoded_data)):
            rx_data.extend(await tb.uart_sink.read())

        tb.log.info("Sended data: %s", test_data)
        tb.log.info("Received (encoded) data: %s", rx_data)
        tb.log.info("Parsed data: %s", cobs_decode(rx_data[:-1]))

        assert tb.uart_sink.empty()
        assert cobs_decode(rx_data[:-1]) == test_data

        await Timer(100, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_rx(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)
    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_data in [payload_data(x) for x in payload_lengths()]:
        await tb.uart_source.write(CobsonFrame.cobs_encode(0, test_data)+b'\x00')

        rx_data = bytearray()
        while len(rx_data) < len(test_data):
            rx_data.extend(await tb.axi_sink.read())

        tb.log.info("Readed data: %s", rx_data)

        assert tb.axi_sink.empty()
        assert rx_data == test_data

        await Timer(100, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs_payload(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 16)) + [32, 128, 1024]


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    for test in [run_test_rx, run_test_rx]:
        factory = TestFactory(test)
        factory.add_option("payload_lengths", [size_list])
        factory.add_option("payload_data", [incrementing_payload, prbs_payload])
        factory.add_option("idle_inserter", [None, cycle_pause])
        factory.add_option("backpressure_inserter", [None, cycle_pause])
        factory.generate_tests()
