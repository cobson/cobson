#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, Timer
from cocotb.regression import TestFactory

from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink
from cocotbext.uart import UartSink, UartSource

from crc import CrcCalculator, Crc8

import sys
sys.path.append('./../../api/Python')
from CobsonFrame import CobsonFrame

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 2, units="ns").start())

        self.axi_input = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.axi_transport = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)
        self.axi_response = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis_cmd"), dut.iclk, dut.irst)

    async def reset(self):
        self.dut.irst.setimmediatevalue(1)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)

    def set_idle_generator(self, generator=None):
        if generator:
            self.axi_input.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.axi_transport.set_pause_generator(generator())


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None, source=None):
    tb = TB(dut)
    crc_calculator = CrcCalculator(Crc8.CCITT)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_data in [payload_data(x) for x in payload_lengths()]:
        cmd = os.urandom(1)
        addr = os.urandom(1)
        # Form AXI frame
        test_frame = AxiStreamFrame(test_data)
        test_frame.tuser = cmd

        dut.iaddress.value = int.from_bytes(addr, "big")

        # Switch data source options
        if (source == 0):
            await tb.axi_input.write(test_frame)
        elif (source == 1):
            await tb.axi_response.write(test_frame)
        else:
            # Random data source for stress test
            rnd_source = random.randrange(0, 2)

            if (rnd_source == 0):
                await tb.axi_input.write(test_frame)
            elif (rnd_source == 1):
                await tb.axi_response.write(test_frame)

        rx_data = await tb.axi_transport.recv()

        decoded = CobsonFrame.cobson_decode(0, rx_data.tdata)
        checksum = bytes([crc_calculator.calculate_checksum(rx_data.tdata[:-1])])

        assert tb.axi_transport.empty()
        assert decoded['data'] == test_data
        assert decoded['crc'] == checksum
        assert decoded['cmd'] == cmd
        assert decoded['addr'] == addr

        await Timer(1, 'us')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs_payload(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 16)) + [32, 255]
    # return list(range(4, 6))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    factory = TestFactory(run_test)
    factory.add_option("source", [0, 1, None])
    factory.add_option("payload_lengths", [size_list])
    factory.add_option("payload_data", [incrementing_payload, prbs_payload])
    factory.add_option("idle_inserter", [None, cycle_pause])
    factory.add_option("backpressure_inserter", [None, cycle_pause])
    factory.generate_tests()
    