#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, Timer
from cocotb.regression import TestFactory

from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink
from cocotbext.uart import UartSink, UartSource

import sys
sys.path.append('./../../api/Python')
from CobsonFrame import CobsonFrame

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 2, units="ns").start())

        self.axi_input = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.axi_output = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)

    async def reset(self):
        self.dut.irst.setimmediatevalue(1)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)

    def set_idle_generator(self, generator=None):
        if generator:
            self.axi_input.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.axi_output.set_pause_generator(generator())


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None, addr=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    dut.iaddress.value = int.from_bytes(addr, "big")

    for test_data in [payload_data(x) for x in payload_lengths()]:
        cmd = os.urandom(1)
        tx_data = CobsonFrame.cobson_encode(0, test_data, addr, cmd)

        await tb.axi_input.write(tx_data)

        rx_data = await tb.axi_output.recv()

        assert tb.axi_output.empty()
        assert rx_data.tdata == test_data
        assert bytes([rx_data.tuser]) == cmd

        await Timer(1, 'us')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_wrong_addr(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None, addr=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    int_addr = int.from_bytes(addr, "big")

    dut.iaddress.value = int_addr

    for test_data in [payload_data(x) for x in payload_lengths()]:
        cmd = os.urandom(1)
        rnd_addr = random.choice(range(int_addr+1, 255))
        tx_data = CobsonFrame.cobson_encode(0, test_data, bytes([rnd_addr]), cmd)

        await tb.axi_input.write(tx_data)
        await Timer(1, 'us')

        assert tb.axi_output.empty()
        await Timer(1, 'us')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_wrong_crc(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None, addr=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    dut.iaddress.value = int.from_bytes(addr, "big")

    for test_data in [payload_data(x) for x in payload_lengths()]:
        cmd = os.urandom(1)
        tx_data = CobsonFrame.cobson_encode(0, test_data, addr, cmd)

        # make CRC wrong
        wrong_crc = tx_data[-1]+1
        tx_data[-1:] = bytes([wrong_crc])

        await tb.axi_input.write(tx_data)
        await Timer(1, 'us')

        assert tb.axi_output.empty()
        await Timer(1, 'us')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_bunch(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None, addr=None):
    tb = TB(dut)
    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    dut.iaddress.value = int.from_bytes(addr, "big")

    # Pause output interface
    tb.axi_output.pause = True

    tx_array = []
    for test_data in [payload_data(x) for x in payload_lengths()]:
        cmd = os.urandom(1)
        tx_frame = CobsonFrame.cobson_encode(0, test_data, addr, cmd)
        tx_array.append([test_data, cmd])
        await tb.axi_input.write(tx_frame)

    # Run output interface back
    await Timer(1, 'us')
    tb.axi_output.pause = False

    # Read data from buffer
    for data, cmd in tx_array:
        rx_frame = await tb.axi_output.recv()

        assert rx_frame.tdata == data
        assert bytes([rx_frame.tuser]) == cmd

    assert tb.axi_output.empty()

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs_payload(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 16)) + [32, 256-4] # -4 because of address, command, size and CRC words
    # return list(range(4, 6))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    for test in [run_test, run_test_wrong_addr, run_test_wrong_crc, run_test_bunch]:
        factory = TestFactory(test)
        factory.add_option("payload_lengths", [size_list])
        factory.add_option("payload_data", [incrementing_payload, prbs_payload])
        factory.add_option("addr", [b'\x00'])
        factory.add_option("idle_inserter", [None, cycle_pause])
        factory.add_option("backpressure_inserter", [None, cycle_pause])
        factory.generate_tests()
