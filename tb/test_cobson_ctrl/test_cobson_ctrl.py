#!/usr/bin/env python

import itertools
import logging
import os
import random

import cocotb_test.simulator

import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, Timer
from cocotb.regression import TestFactory

from cocotbext.axi import AxiStreamBus, AxiStreamFrame, AxiStreamSource, AxiStreamSink
from cocotbext.uart import UartSink, UartSource

import sys
sys.path.append('./../../api/Python')
from CobsonFrame import CobsonFrame

#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
class TB(object):
    def __init__(self, dut):
        self.dut = dut

        self.log = logging.getLogger("cocotb.tb")
        self.log.setLevel(logging.DEBUG)

        cocotb.fork(Clock(dut.iclk, 2, units="ns").start())

        self.axi_input = AxiStreamSource(AxiStreamBus.from_prefix(dut, "s_axis"), dut.iclk, dut.irst)
        self.axi_output = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis"), dut.iclk, dut.irst)
        self.axi_output_cmd = AxiStreamSink(AxiStreamBus.from_prefix(dut, "m_axis_cmd"), dut.iclk, dut.irst)

    async def reset(self):
        self.dut.irst.setimmediatevalue(1)
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)
        self.dut.irst.value = 0
        await RisingEdge(self.dut.iclk)
        await RisingEdge(self.dut.iclk)

    def set_idle_generator(self, generator=None):
        if generator:
            self.axi_input.set_pause_generator(generator())

    def set_backpressure_generator(self, generator=None):
        if generator:
            self.axi_output.set_pause_generator(generator())
            self.axi_output_cmd.set_pause_generator(generator())


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_echo(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_data in [payload_data(x) for x in payload_lengths()]:
        test_frame = AxiStreamFrame(test_data)
        test_frame.tuser = 253

        await tb.axi_input.write(test_frame)
        rx_frame = await tb.axi_output_cmd.recv()

        assert tb.axi_output.empty()
        assert tb.axi_output_cmd.empty()
        assert rx_frame.tdata == test_frame.tdata
        assert rx_frame.tuser == test_frame.tuser

        await Timer(10, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_user(dut, payload_lengths=None, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    commands = list(range(0, 248))

    for test_data in [payload_data(x) for x in payload_lengths()]:
        test_frame = AxiStreamFrame(test_data)
        test_frame.tuser = random.choices(commands)[0]

        await tb.axi_input.write(test_frame)
        rx_frame = await tb.axi_output.recv()

        assert tb.axi_output.empty()
        assert tb.axi_output_cmd.empty()
        assert rx_frame.tdata == test_frame.tdata
        assert rx_frame.tuser == test_frame.tuser

        await Timer(10, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_address(dut, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_addr in payload_data:
        # Set address
        test_frame = AxiStreamFrame(bytes([test_addr]))
        test_frame.tuser = 249
        await tb.axi_input.write(test_frame)

        # Set address request
        test_frame = AxiStreamFrame(bytes([test_addr]))
        test_frame.tuser = 250
        await tb.axi_input.write(test_frame)
        # Get response
        rx_frame = await tb.axi_output_cmd.recv()

        assert tb.axi_output.empty()
        assert tb.axi_output_cmd.empty()
        assert rx_frame.tdata == test_frame.tdata
        assert rx_frame.tuser == test_frame.tuser

        await Timer(10, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
async def run_test_nope(dut, payload_data=None, idle_inserter=None, backpressure_inserter=None):
    tb = TB(dut)

    await tb.reset()

    tb.set_idle_generator(idle_inserter)
    tb.set_backpressure_generator(backpressure_inserter)

    for test_addr in payload_data:
        test_frame = AxiStreamFrame(bytes([test_addr]))
        test_frame.tuser = 255
        await tb.axi_input.write(test_frame)

        for _ in range(32):
            await RisingEdge(dut.iclk)

        assert tb.axi_output.empty()
        assert tb.axi_output_cmd.empty()
        assert rx_frame.tdata == test_frame.tdata
        assert rx_frame.tuser == test_frame.tuser

        await Timer(10, 'ns')

    await RisingEdge(dut.iclk)
    await RisingEdge(dut.iclk)


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs31(state=0x7fffffff):
    while True:
        for i in range(8):
            if bool(state & 0x08000000) ^ bool(state & 0x40000000):
                state = ((state & 0x3fffffff) << 1) | 1
            else:
                state = (state & 0x3fffffff) << 1
        yield state & 0xff


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def prbs_payload(length):
    gen = prbs31()
    return bytearray([next(gen) for x in range(length)])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def incrementing_payload(length):
    return bytearray(itertools.islice(itertools.cycle(range(256)), length))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def size_list():
    return list(range(1, 16)) + [32, 256-4] # -4 because of address, command, size and CRC words
    # return list(range(4, 6))


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
def cycle_pause():
    return itertools.cycle([1, 1, 1, 0])


#----------------------------------------------------------------------------------
#
#----------------------------------------------------------------------------------
# cocotb-test
if cocotb.SIM_NAME:
    for test in [run_test_user, run_test_echo, run_test_nope]:
        factory = TestFactory(test)
        factory.add_option("payload_lengths", [size_list])
        factory.add_option("payload_data", [incrementing_payload, prbs_payload])
        factory.add_option("idle_inserter", [None, cycle_pause])
        factory.add_option("backpressure_inserter", [None, cycle_pause])
        factory.generate_tests()

    factory = TestFactory(run_test_address)
    factory.add_option("payload_data", [list(range(1, 255))])
    factory.add_option("idle_inserter", [None, cycle_pause])
    factory.add_option("backpressure_inserter", [None, cycle_pause])
    factory.generate_tests()
