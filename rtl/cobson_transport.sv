/*
  logic        tr__irst          ;
  logic        tr__iclk          ;
  logic [15:0] tr__idivider      ;
  logic        tr__s_axis_tvalid ;
  logic  [7:0] tr__s_axis_tdata  ;
  logic  [7:0] tr__s_axis_tuser  ;
  logic        tr__s_axis_tlast  ;
  logic        tr__s_axis_tready ;
  logic        tr__m_axis_tvalid ;
  logic  [7:0] tr__m_axis_tdata  ;
  logic        tr__m_axis_tlast  ;
  logic        tr__m_axis_tuser  ;
  logic        tr__m_axis_tready ;
  logic        tr__iuart_rx      ;
  logic        tr__ouart_tx      ;
  tr_stat_t    tr__ostat         ;

  cobson_transport
  tr
  (
    .irst          (tr__irst               ) ,
    .iclk          (tr__iclk               ) ,
    .idivider      (tr__idivider           ) ,
    .s_axis_tvalid (tr__s_axis_tvalid      ) ,
    .s_axis_tdata  (tr__s_axis_tdata       ) ,
    .s_axis_tuser  (tr__s_axis_tuser       ) ,
    .s_axis_tlast  (tr__s_axis_tlast       ) ,
    .s_axis_tready (tr__s_axis_tready      ) ,
    .m_axis_tvalid (tr__m_axis_tvalid      ) ,
    .m_axis_tdata  (tr__m_axis_tdata       ) ,
    .m_axis_tuser  (tr__m_axis_tuser       ) ,
    .m_axis_tlast  (tr__m_axis_tlast       ) ,
    .m_axis_tready (tr__m_axis_tready      ) ,
    .iuart_rx      (tr__iuart_rx           ) ,
    .ouart_tx      (tr__ouart_tx           ) ,
    .ostat         (tr__ostat              )
  );

  assign tr__irst          = '0;
  assign tr__iclk          = '0;
  assign tr__idivider      = '0;
  assign tr__s_axis_tvalid = '0;
  assign tr__s_axis_tdata  = '0;
  assign tr__s_axis_tuser  = '0;
  assign tr__s_axis_tlast  = '0;
  assign tr__m_axis_tready = '0;
  assign tr__iuart_rx      = '0;
*/ 



module cobson_transport
(
  input  logic        irst          ,
  input  logic        iclk          ,
  input  logic [15:0] idivider      ,
  //
  input  logic        s_axis_tvalid ,
  input  logic  [7:0] s_axis_tdata  ,
  input  logic  [7:0] s_axis_tuser  ,
  input  logic        s_axis_tlast  ,
  output logic        s_axis_tready ,
  //
  output logic        m_axis_tvalid ,
  output logic  [7:0] m_axis_tdata  ,
  output logic        m_axis_tuser  ,
  output logic        m_axis_tlast  ,
  input  logic        m_axis_tready ,
  //
  input  logic        iuart_rx      ,
  output logic        ouart_tx      ,
  //
  output tr_stat_t    ostat
);

  parameter int DATA_WIDTH = 8;
  parameter int WORD_WIDTH = DATA_WIDTH;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic                  enc__iclk           ;
  logic                  enc__irst           ;
  logic            [7:0] enc__s_axis_tdata   ;
  logic                  enc__s_axis_tvalid  ;
  logic                  enc__s_axis_tready  ;
  logic                  enc__s_axis_tlast   ;
  logic                  enc__s_axis_tuser   ;
  logic                  enc__m_axis_tuser   ;
  logic            [7:0] enc__m_axis_tdata   ;
  logic                  enc__m_axis_tvalid  ;
  logic                  enc__m_axis_tready  ;
  logic                  enc__m_axis_tlast   ;

  logic                  dec__iclk           ;
  logic                  dec__irst           ;
  logic            [7:0] dec__s_axis_tdata   ;
  logic                  dec__s_axis_tvalid  ;
  logic                  dec__s_axis_tready  ;
  logic                  dec__s_axis_tlast   ;
  logic                  dec__s_axis_tuser   ;
  logic            [7:0] dec__m_axis_tdata   ;
  logic                  dec__m_axis_tvalid  ;
  logic                  dec__m_axis_tready  ;
  logic                  dec__m_axis_tlast   ;
  logic                  dec__m_axis_tuser   ;

  logic                  tx__iclk            ;
  logic                  tx__irst            ;
  logic [WORD_WIDTH-1:0] tx__s_axis_tdata    ;
  logic                  tx__s_axis_tvalid   ;
  logic                  tx__s_axis_tready   ;
  logic           [15:0] tx__idivider        ;
  logic                  tx__otx             ;

  logic                  rx__iclk            ;
  logic                  rx__irst            ;
  logic [WORD_WIDTH-1:0] rx__m_axis_tdata    ;
  logic                  rx__m_axis_tvalid   ;
  logic                  rx__m_axis_tready   ;
  logic           [15:0] rx__idivider        ;
  logic                  rx__irx             ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign rx__iclk          = iclk;
  assign rx__irst          = irst;
  assign rx__m_axis_tready = dec__s_axis_tready;
  assign rx__idivider      = idivider;
  assign rx__irx           = iuart_rx;

  sv_uart_rx
  #(
    . DATA_WIDTH    (WORD_WIDTH             ) ,
    . IN_PIPE       (5                      ) 
  )
  rx
  (
    .iclk           (rx__iclk               ) ,
    .irst           (rx__irst               ) ,
    .m_axis_tdata   (rx__m_axis_tdata       ) ,
    .m_axis_tvalid  (rx__m_axis_tvalid      ) ,
    .m_axis_tready  (rx__m_axis_tready      ) ,
    .idivider       (rx__idivider           ) ,
    .irx            (rx__irx                ) 
  );

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic    tlast;

  always_ff@(posedge iclk) begin
    if (irst)  tlast <= 1'b0;
    else       tlast <= rx__m_axis_tvalid && (rx__m_axis_tdata == 8'd0);
  end

  assign dec__iclk          = iclk;
  assign dec__irst          = irst;
  assign dec__s_axis_tdata  = rx__m_axis_tdata;
  assign dec__s_axis_tvalid = rx__m_axis_tvalid;
  assign dec__s_axis_tlast  = tlast;
  assign dec__s_axis_tuser  = 1'b0;
  assign dec__m_axis_tready = m_axis_tready;

  axis_cobs_dec
  dec
  (
    .iclk          (dec__iclk               ) ,
    .irst          (dec__irst               ) ,
    .s_axis_tdata  (dec__s_axis_tdata       ) ,
    .s_axis_tvalid (dec__s_axis_tvalid      ) ,
    .s_axis_tready (dec__s_axis_tready      ) ,
    .s_axis_tlast  (dec__s_axis_tlast       ) ,
    .s_axis_tuser  (dec__s_axis_tuser       ) ,
    .m_axis_tdata  (dec__m_axis_tdata       ) ,
    .m_axis_tvalid (dec__m_axis_tvalid      ) ,
    .m_axis_tready (dec__m_axis_tready      ) ,
    .m_axis_tlast  (dec__m_axis_tlast       ) ,
    .m_axis_tuser  (dec__m_axis_tuser       ) 
  );

  assign m_axis_tvalid = dec__m_axis_tvalid;
  assign m_axis_tdata = dec__m_axis_tdata;
  assign m_axis_tuser = dec__m_axis_tuser;
  assign m_axis_tlast = dec__m_axis_tlast;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign enc__iclk          = iclk;
  assign enc__irst          = irst;
  assign enc__s_axis_tdata  = s_axis_tdata;
  assign enc__s_axis_tvalid = s_axis_tvalid;
  assign enc__s_axis_tlast  = s_axis_tlast;
  assign enc__s_axis_tuser  = s_axis_tuser;
  assign enc__m_axis_tready = tx__s_axis_tready;

  axis_cobs_enc
  #(
    . ZERO_APPEND  (1                       ) 
  )
  enc
  (
    .iclk          (enc__iclk               ) ,
    .irst          (enc__irst               ) ,
    .s_axis_tdata  (enc__s_axis_tdata       ) ,
    .s_axis_tvalid (enc__s_axis_tvalid      ) ,
    .s_axis_tready (enc__s_axis_tready      ) ,
    .s_axis_tlast  (enc__s_axis_tlast       ) ,
    .s_axis_tuser  (enc__s_axis_tuser       ) ,
    .m_axis_tuser  (enc__m_axis_tuser       ) ,
    .m_axis_tdata  (enc__m_axis_tdata       ) ,
    .m_axis_tvalid (enc__m_axis_tvalid      ) ,
    .m_axis_tready (enc__m_axis_tready      ) ,
    .m_axis_tlast  (enc__m_axis_tlast       ) 
  );

  assign s_axis_tready = enc__s_axis_tready;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign tx__iclk          = iclk;
  assign tx__irst          = irst;
  assign tx__s_axis_tdata  = enc__m_axis_tdata;
  assign tx__s_axis_tvalid = enc__m_axis_tvalid;
  assign tx__idivider      = idivider;

  sv_uart_tx
  #(
    . DATA_WIDTH    (WORD_WIDTH             ) ,
    . STOP_BITS     (1                      ) 
  )
  tx
  (
    .iclk           (tx__iclk               ) ,
    .irst           (tx__irst               ) ,
    .s_axis_tdata   (tx__s_axis_tdata       ) ,
    .s_axis_tvalid  (tx__s_axis_tvalid      ) ,
    .s_axis_tready  (tx__s_axis_tready      ) ,
    .idivider       (tx__idivider           ) ,
    .otx            (tx__otx                ) 
  );

  assign ouart_tx = tx__otx;

endmodule
