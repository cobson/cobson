`ifndef COBSON_HEADER
`define COBSON_HEADER

  localparam int VER = 0;
  localparam int SUB = 0;
  localparam int REV = 1;

  enum logic [7:0] {
    CMD_NOPE    = 8'd255,
    CMD_RESET   = 8'd254,
    CMD_ECHO    = 8'd253,
    CMD_INFO    = 8'd252,
    CMD_GET_ERR = 8'd251,
    CMD_GET_ADR = 8'd250,
    CMD_SET_ADR = 8'd249
  } COMMAND_MAP;

  typedef struct packed {
    logic         err_crc;
    logic         err_addr;
    logic         err_size;
    logic         err_ovrflw;
    logic         err_timeout;
  } rx_stat_t;

  typedef struct packed {
    logic         err_ovrflw_data;
    logic         err_ovrflw_cmd;
  } tx_stat_t;

  typedef struct packed {
    logic         err_ovrflw_data;
    logic         err_ovrflw_cmd;
  } tr_stat_t;

  typedef struct packed {
    rx_stat_t         rx;
    tx_stat_t         tx;
    tr_stat_t         tr;
  } stat_t;

  // CRC polynomial coefficients: x^8 + x^2 + x + 1
  //                              0x7 (hex)
  // CRC width:                   8 bits
  // CRC shift direction:         left (big endian)
  // Input word width:            8 bits
  function automatic [7:0] crc8;
      input [7:0] idat;
      input [7:0] data;
  begin
      crc8[0] = (data[0] ^ data[6] ^ data[7] ^ idat[0] ^ idat[6] ^ idat[7]);
      crc8[1] = (data[0] ^ data[1] ^ data[6] ^ idat[0] ^ idat[1] ^ idat[6]);
      crc8[2] = (data[0] ^ data[1] ^ data[2] ^ data[6] ^ idat[0] ^ idat[1] ^ idat[2] ^ idat[6]);
      crc8[3] = (data[1] ^ data[2] ^ data[3] ^ data[7] ^ idat[1] ^ idat[2] ^ idat[3] ^ idat[7]);
      crc8[4] = (data[2] ^ data[3] ^ data[4] ^ idat[2] ^ idat[3] ^ idat[4]);
      crc8[5] = (data[3] ^ data[4] ^ data[5] ^ idat[3] ^ idat[4] ^ idat[5]);
      crc8[6] = (data[4] ^ data[5] ^ data[6] ^ idat[4] ^ idat[5] ^ idat[6]);
      crc8[7] = (data[5] ^ data[6] ^ data[7] ^ idat[5] ^ idat[6] ^ idat[7]);
  end
  endfunction

`endif