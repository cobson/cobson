/*



  parameter int DATA_BUF_W = 8;



  logic       tx__irst              ;
  logic       tx__iclk_in           ;
  logic       tx__iclk              ;
  logic [7:0] tx__iaddress          ;
  logic       tx__s_axis_tvalid     ;
  logic [7:0] tx__s_axis_tdata      ;
  logic [7:0] tx__s_axis_tuser      ;
  logic       tx__s_axis_tlast      ;
  logic       tx__s_axis_tready     ;
  logic       tx__m_axis_tvalid     ;
  logic [7:0] tx__m_axis_tdata      ;
  logic [7:0] tx__m_axis_tuser      ;
  logic       tx__m_axis_tlast      ;
  logic       tx__m_axis_tready     ;
  logic       tx__s_axis_cmd_tvalid ;
  logic [7:0] tx__s_axis_cmd_tdata  ;
  logic [7:0] tx__s_axis_cmd_tuser  ;
  logic       tx__s_axis_cmd_tlast  ;
  logic       tx__s_axis_cmd_tready ;
  tx_stat_t   tx__ostat             ;



  cobson_tx
  #(
    . DATA_BUF_W       (DATA_BUF_W      ) 
  )
  cobson_tx
  (
    .irst              (tx__irst                   ) ,
    .iclk_in           (tx__iclk_in                ) ,
    .iclk              (tx__iclk                   ) ,
    .iaddress          (tx__iaddress               ) ,
    .s_axis_tvalid     (tx__s_axis_tvalid          ) ,
    .s_axis_tdata      (tx__s_axis_tdata           ) ,
    .s_axis_tuser      (tx__s_axis_tuser           ) ,
    .s_axis_tlast      (tx__s_axis_tlast           ) ,
    .s_axis_tready     (tx__s_axis_tready          ) ,
    .m_axis_tvalid     (tx__m_axis_tvalid          ) ,
    .m_axis_tdata      (tx__m_axis_tdata           ) ,
    .m_axis_tlast      (tx__m_axis_tlast           ) ,
    .m_axis_tready     (tx__m_axis_tready          ) ,
    .s_axis_cmd_tvalid (tx__s_axis_cmd_tvalid      ) ,
    .s_axis_cmd_tdata  (tx__s_axis_cmd_tdata       ) ,
    .s_axis_cmd_tuser  (tx__s_axis_cmd_tuser       ) ,
    .s_axis_cmd_tlast  (tx__s_axis_cmd_tlast       ) ,
    .s_axis_cmd_tready (tx__s_axis_cmd_tready      ) 
  );


  assign tx__irst              = '0;
  assign tx__iclk_in           = '0;
  assign tx__iclk              = '0;
  assign tx__iaddress          = '0;
  assign tx__s_axis_tvalid     = '0;
  assign tx__s_axis_tdata      = '0;
  assign tx__s_axis_tlast      = '0;
  assign tx__s_axis_tuser      = '0;
  assign tx__m_axis_tready     = '0;
  assign tx__s_axis_cmd_tvalid = '0;
  assign tx__s_axis_cmd_tdata  = '0;
  assign tx__s_axis_cmd_tlast  = '0;



*/ 

`include "cobson.svh"

module cobson_tx
#(
  parameter int DATA_BUF_W = 8 ,
  parameter int RESP_BUF_W = 8
)
(
  input  logic       irst              ,
  input  logic       iclk_in           ,
  input  logic       iclk              ,
  input  logic [7:0] iaddress          ,
  output tx_stat_t   ostat             ,
  // from user
  input  logic       s_axis_tvalid     ,
  input  logic [7:0] s_axis_tdata      ,
  input  logic [7:0] s_axis_tuser      ,
  input  logic       s_axis_tlast      ,
  output logic       s_axis_tready     ,
 // from rx datapath
  input  logic       s_axis_cmd_tvalid ,
  input  logic [7:0] s_axis_cmd_tdata  ,
  input  logic [7:0] s_axis_cmd_tuser  ,
  input  logic       s_axis_cmd_tlast  ,
  output logic       s_axis_cmd_tready ,
  // to transport
  output logic       m_axis_tvalid     ,
  output logic [7:0] m_axis_tdata      ,
  output logic       m_axis_tlast      ,
  input  logic       m_axis_tready
);

  localparam int pDATA_WORDS = 2**DATA_BUF_W;
  localparam int pRESP_WORDS = 2**RESP_BUF_W;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic                           db__s_axis_tready ;
  logic                     [7:0] db__m_axis_tdata  ;
  logic                     [7:0] db__m_axis_tuser  ;
  logic                           db__m_axis_tready ;
  logic                           db__m_axis_tvalid ;
  logic                           db__m_axis_tlast  ;
  logic [$clog2(pDATA_WORDS)-1:0] db__used          ;
  logic                           db__empty         ;

  logic                           rb__s_axis_tready ;
  logic                     [7:0] rb__m_axis_tdata  ;
  logic                     [7:0] rb__m_axis_tuser  ;
  logic                           rb__m_axis_tready ;
  logic                           rb__m_axis_tvalid ;
  logic                           rb__m_axis_tlast  ;
  logic [$clog2(pRESP_WORDS)-1:0] rb__used          ;
  logic                           rb__empty         ;

  enum logic [3:0] {
    ST_IDLE = 4'd0,
    ST_HEAD = 4'd1,
    ST_BODY = 4'd2,
    ST_TAIL = 4'd3
  } state;

  logic           flag_resp_tx;
  logic           flag_data_tx;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Data buffer
  assign db__m_axis_tready = (state == ST_BODY) && m_axis_tready && flag_data_tx;

  axis_sfifo
  #(
    . DATA_WIDTH   (8                  ) ,
    . DEPTH        (pDATA_WORDS        ) ,
    . FRAME        (1                  ) ,
    . USER_ENA     (1                  ) ,
    . USER_WIDTH   (8                  ) ,
    . LAST_ENA     (1                  )
  )
  data_buf
  (
    .iclk          (iclk               ) ,
    .irst          (irst               ) ,
    .s_axis_tdata  (s_axis_tdata       ) ,
    .s_axis_tvalid (s_axis_tvalid      ) ,
    .s_axis_tlast  (s_axis_tlast       ) ,
    .s_axis_tuser  (s_axis_tuser       ) ,
    .s_axis_tready (db__s_axis_tready  ) ,
    .m_axis_tdata  (db__m_axis_tdata   ) ,
    .m_axis_tuser  (db__m_axis_tuser   ) ,
    .m_axis_tready (db__m_axis_tready  ) ,
    .m_axis_tvalid (db__m_axis_tvalid  ) ,
    .m_axis_tlast  (db__m_axis_tlast   ) ,
    .used          (db__used           ) ,
    .empty         (db__empty          ) ,
    .full          () ,
    .overflow      () ,
    .underflow     () 
  );

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Repsonse buffer
  assign rb__m_axis_tready = (state == ST_BODY) && m_axis_tready && flag_resp_tx;

  axis_sfifo
  #(
    . DATA_WIDTH    (8                  ) ,
    . DEPTH         (pRESP_WORDS        ) ,
    . FRAME         (1                  ) ,
    . USER_ENA      (1                  ) ,
    . USER_WIDTH    (8                  ) ,
    . LAST_ENA      (1                  )
  )
  resp_buf
  (
    .iclk           (iclk               ) ,
    .irst           (irst               ) ,
    .s_axis_tdata   (s_axis_cmd_tdata   ) ,
    .s_axis_tvalid  (s_axis_cmd_tvalid  ) ,
    .s_axis_tlast   (s_axis_cmd_tlast   ) ,
    .s_axis_tuser   (s_axis_cmd_tuser   ) ,
    .s_axis_tready  (rb__s_axis_tready  ) ,
    .m_axis_tdata   (rb__m_axis_tdata   ) ,
    .m_axis_tuser   (rb__m_axis_tuser   ) ,
    .m_axis_tready  (rb__m_axis_tready  ) ,
    .m_axis_tvalid  (rb__m_axis_tvalid  ) ,
    .m_axis_tlast   (rb__m_axis_tlast   ) ,
    .used           (rb__used           ) ,
    .empty          (rb__empty          ) ,
    .full           () ,
    .overflow       () ,
    .underflow      () 
  );

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  logic     [1:0] cnt_head;
  logic           fsm_start;

  assign data_val = m_axis_tready || ~m_axis_tvalid;
  assign fsm_tlast = (flag_data_tx) ? (db__m_axis_tlast) : (rb__m_axis_tlast);
  assign fsm_start = ~rb__empty || ~db__empty;

  always_ff @(posedge iclk) begin
    case (state)
      ST_IDLE : if (fsm_start)                     state <= ST_HEAD;
      ST_HEAD : if (cnt_head == 'd2 && data_val)   state <= ST_BODY;
      ST_BODY : if (fsm_tlast && data_val)         state <= ST_TAIL;
      ST_TAIL : if (m_axis_tready)                 state <= ST_IDLE;
      default : state <= ST_IDLE;
    endcase
  end

  always_ff @(posedge iclk) begin
    if (state == ST_IDLE)            cnt_head <= '0;
    else if (data_val & ~&cnt_head)  cnt_head <= cnt_head + 1'b1;

    if (irst) begin
      {flag_resp_tx, flag_data_tx} <= 2'b00;
    end else if (state == ST_IDLE) begin
      if (~rb__empty)
        {flag_resp_tx, flag_data_tx} <= 2'b10;
      else if (~db__empty)
        {flag_resp_tx, flag_data_tx} <= 2'b01;
    end else if (state == ST_TAIL) begin
      {flag_resp_tx, flag_data_tx} <= 2'b00;
    end
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  logic    [7:0] tx_dat;
  logic    [7:0] crc;

  // CRC calc
  always_ff @(posedge iclk) begin
    if (irst)                   crc <= '0;
    else if (state == ST_IDLE)  crc <= '0;
      else if (data_val)        crc <= crc8(tx_dat, crc);
  end

  // Data mux
  logic  [7:0] data_used;
  logic  [7:0] resp_used;
  always_ff @(posedge iclk) begin
    data_used <= db__used + 1'b1;
    resp_used <= rb__used + 1'b1;
  end

  always_comb begin
    if (cnt_head == 2'd0)
      tx_dat = iaddress;
    else if (cnt_head == 2'd1)
      tx_dat = (flag_resp_tx) ? (rb__m_axis_tuser) : (db__m_axis_tuser);
    else if (cnt_head == 2'd2)
      tx_dat = (flag_resp_tx) ? (resp_used) : (data_used);
    else if (state == ST_BODY)
      tx_dat = (flag_resp_tx) ? (rb__m_axis_tdata) : (db__m_axis_tdata);
    else
      tx_dat = crc;
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Output interface
  always_ff @(posedge iclk) begin
    if (irst)                     m_axis_tvalid <= 1'b0;
    else if (state == ST_BODY)    m_axis_tvalid <= (flag_resp_tx) ? (rb__m_axis_tvalid) : (db__m_axis_tvalid);
      else if (state != ST_IDLE)  m_axis_tvalid <= 1'b1;
        else if (m_axis_tready)   m_axis_tvalid <= 1'b0;

    if (data_val)
      m_axis_tdata <= tx_dat;

    if (data_val)
      m_axis_tlast <= (state == ST_TAIL);
  end

  assign s_axis_tready = db__s_axis_tready;
  assign s_axis_cmd_tready = rb__s_axis_tready;

endmodule
