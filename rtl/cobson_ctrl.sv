/*
  parameter int pBUF_W = 8;

  logic           ctrl__irst              ;
  logic           ctrl__iclk              ;
  stat_t          ctrl__istat             ;
  logic           ctrl__oreset            ;
  logic     [7:0] ctrl__oaddress          ;
  logic           ctrl__s_axis_tvalid     ;
  logic     [7:0] ctrl__s_axis_tdata      ;
  logic     [7:0] ctrl__s_axis_tuser      ;
  logic           ctrl__s_axis_tlast      ;
  logic           ctrl__s_axis_tready     ;
  logic           ctrl__m_axis_tvalid     ;
  logic     [7:0] ctrl__m_axis_tdata      ;
  logic     [7:0] ctrl__m_axis_tuser      ;
  logic           ctrl__m_axis_tlast      ;
  logic           ctrl__m_axis_tready     ;
  logic           ctrl__m_axis_cmd_tvalid ;
  logic     [7:0] ctrl__m_axis_cmd_tdata  ;
  logic     [7:0] ctrl__m_axis_cmd_tuser  ;
  logic           ctrl__m_axis_cmd_tlast  ;
  logic           ctrl__m_axis_cmd_tready ;

  cobson_ctrl
  #(
    . pBUF_W    (pBUF_W      ) 
  )
  ctrl
  (
    .irst              (ctrl__irst                   ) ,
    .iclk              (ctrl__iclk                   ) ,
    .istat             (ctrl__istat                  ) ,
    .oaddress          (ctrl__oaddress               ) ,
    .oreset            (ctrl__oreset                 ) ,
    .s_axis_tvalid     (ctrl__s_axis_tvalid          ) ,
    .s_axis_tdata      (ctrl__s_axis_tdata           ) ,
    .s_axis_tuser      (ctrl__s_axis_tuser           ) ,
    .s_axis_tlast      (ctrl__s_axis_tlast           ) ,
    .s_axis_tready     (ctrl__s_axis_tready          ) ,
    .m_axis_tvalid     (ctrl__m_axis_tvalid          ) ,
    .m_axis_tdata      (ctrl__m_axis_tdata           ) ,
    .m_axis_tuser      (ctrl__m_axis_tuser           ) ,
    .m_axis_tlast      (ctrl__m_axis_tlast           ) ,
    .m_axis_tready     (ctrl__m_axis_tready          ) ,
    .m_axis_cmd_tvalid (ctrl__m_axis_cmd_tvalid      ) ,
    .m_axis_cmd_tdata  (ctrl__m_axis_cmd_tdata       ) ,
    .m_axis_cmd_tuser  (ctrl__m_axis_cmd_tuser       ) ,
    .m_axis_cmd_tlast  (ctrl__m_axis_cmd_tlast       ) ,
    .m_axis_cmd_tready (ctrl__m_axis_cmd_tready      ) 
  );

  assign ctrl__irst              = '0;
  assign ctrl__iclk              = '0;
  assign ctrl__istat             = '0;
  assign ctrl__s_axis_tvalid     = '0;
  assign ctrl__s_axis_tdata      = '0;
  assign ctrl__s_axis_tuser      = '0;
  assign ctrl__s_axis_tlast      = '0;
  assign ctrl__m_axis_tready     = '0;
  assign ctrl__m_axis_cmd_tready = '0;
*/ 

`include "cobson.svh"

module cobson_ctrl
#(
  parameter bit [7:0] DEFAULT_ADDR = 8'h00 ,
  parameter int       BUF_W        = 8
)
(
  input  logic           irst              ,
  input  logic           iclk              ,
  input  stat_t          istat             ,
  output logic           oreset            ,
  output logic     [7:0] oaddress          ,
  //
  input  logic           s_axis_tvalid     ,
  input  logic     [7:0] s_axis_tdata      ,
  input  logic     [7:0] s_axis_tuser      ,
  input  logic           s_axis_tlast      ,
  output logic           s_axis_tready     ,
  //
  output logic           m_axis_tvalid     ,
  output logic     [7:0] m_axis_tdata      ,
  output logic     [7:0] m_axis_tuser      ,
  output logic           m_axis_tlast      ,
  input  logic           m_axis_tready     ,
  //
  output logic           m_axis_cmd_tvalid ,
  output logic     [7:0] m_axis_cmd_tdata  ,
  output logic     [7:0] m_axis_cmd_tuser  ,
  output logic           m_axis_cmd_tlast  ,
  input  logic           m_axis_cmd_tready 
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  logic           proc_done;
  logic           ena_nope;
  logic           ena_echo;
  logic           ena_reset;
  logic           ena_info;
  logic           ena_error;
  logic           ena_get_addr;
  logic           ena_set_addr;
  logic           ena_user_data;
  logic           wr_ena;
  logic     [7:0] address;
  logic     [7:0] last_error = '0;

  enum logic [2:0] {
    ST_IDLE     = 3'd0,
    ST_PREFETCH = 3'd1,
    ST_FETCH    = 3'd2,
    ST_END      = 3'd3
  } state;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  always_ff @(posedge iclk) begin
    if (irst)
      state <= ST_IDLE;
    else case (state)
      // Wait till master is ready
      ST_IDLE     : begin
        if (s_axis_tvalid)
          state <= ST_PREFETCH;
      end

      // Get very first tdata & tuser samples then process it
      ST_PREFETCH : begin
        if (proc_done)
          state <= ST_FETCH;
      end

      // Fetch other part of data
      ST_FETCH    : begin
        if (wr_ena) begin
          // If we writing something into module
          if (s_axis_tlast && s_axis_tvalid && s_axis_tready)
            state <= ST_END;
        end else begin
          // OR if we ask module of response
          if (m_axis_tlast && m_axis_tvalid && m_axis_tready)
            state <= ST_END;
          else if (m_axis_cmd_tlast && m_axis_cmd_tvalid && m_axis_cmd_tready)
            state <= ST_END;
        end
      end

      // Done
      ST_END      : begin
        state <= ST_IDLE;
      end

      default     : state <= ST_IDLE;
    endcase
  end

  // First stage of parsing: is it special command or just user data?
  always_ff @(posedge iclk) begin
    if (irst) begin
      ena_nope <= 1'b0;
      ena_echo <= 1'b0;
      ena_reset <= 1'b0;
      ena_info <= 1'b0;
      ena_error <= 1'b0;
      ena_get_addr <= 1'b0;
      ena_set_addr <= 1'b0;
      wr_ena <= 1'b0;
      ena_user_data <= 1'b0;
    end else if (state == ST_END) begin
      ena_nope <= 1'b0;
      ena_echo <= 1'b0;
      ena_reset <= 1'b0;
      ena_info <= 1'b0;
      ena_error <= 1'b0;
      ena_get_addr <= 1'b0;
      ena_set_addr <= 1'b0;
      wr_ena <= 1'b0;
      ena_user_data <= 1'b0;
    end else if (state == ST_PREFETCH) begin
      ena_nope <= (s_axis_tuser == CMD_NOPE);
      ena_echo <= (s_axis_tuser == CMD_ECHO);
      ena_reset <= (s_axis_tuser == CMD_RESET);
      ena_info <= (s_axis_tuser == CMD_INFO);
      ena_error <= (s_axis_tuser == CMD_GET_ERR);
      ena_get_addr <= (s_axis_tuser == CMD_GET_ADR);
      ena_set_addr <= (s_axis_tuser == CMD_SET_ADR);
      wr_ena <= (s_axis_tuser == CMD_SET_ADR);

      ena_user_data <=  ~(s_axis_tuser == CMD_NOPE)
                      & ~(s_axis_tuser == CMD_ECHO)
                      & ~(s_axis_tuser == CMD_RESET)
                      & ~(s_axis_tuser == CMD_INFO)
                      & ~(s_axis_tuser == CMD_GET_ERR)
                      & ~(s_axis_tuser == CMD_GET_ADR)
                      & ~(s_axis_tuser == CMD_SET_ADR);
    end

    // We have something here
    if (irst)
      proc_done <= 1'b0;
    else
      proc_done <= ena_user_data | ena_echo | ena_reset | ena_info | ena_error | ena_get_addr | ena_set_addr;
  end

  always_ff @(posedge iclk) begin
    if (irst)
      address <= DEFAULT_ADDR;
    else if (ena_set_addr && s_axis_tvalid && s_axis_tready)
      address <= s_axis_tdata;
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  assign oaddress = address;

  assign s_tready = (ena_user_data) ? (m_axis_tready) : (m_axis_cmd_tready);
  assign s_axis_tready = (state == ST_FETCH) && s_tready;

  // Really tricky data to output mux
  //
  // Data output to user path
  always_ff @(posedge iclk) begin
    if (irst) begin
      m_axis_tvalid <= '0;
      m_axis_tdata <= '0;
      m_axis_tuser <= '0;
      m_axis_tlast <= '0;
    end else if (ena_user_data) begin
      if (s_axis_tvalid && s_axis_tready && (state == ST_FETCH))
        m_axis_tvalid <= 1'b1;
      else if (m_axis_tready)
        m_axis_tvalid <= 1'b0;
      
      if (s_axis_tvalid && s_axis_tready && (state == ST_FETCH)) begin
        m_axis_tdata <= s_axis_tdata;
        m_axis_tuser <= s_axis_tuser;
        m_axis_tlast <= s_axis_tlast;
      end
    end else begin
      m_axis_tvalid <= '0;
      m_axis_tdata <= '0;
      m_axis_tuser <= '0;
      m_axis_tlast <= '0;
    end
  end

  // Command output to TX path
  always_ff @(posedge iclk) begin
    if (irst) begin
      m_axis_cmd_tvalid <= '0;
      m_axis_cmd_tdata <= '0;
      m_axis_cmd_tuser <= '0;
      m_axis_cmd_tlast <= '0;
    end else if (~ena_user_data) begin
      if (s_axis_tvalid && s_axis_tready && (state == ST_FETCH)) begin
        if (ena_set_addr)
          m_axis_cmd_tvalid <= 1'b0;
        else
          m_axis_cmd_tvalid <= 1'b1;
      end else if (m_axis_cmd_tready) begin
        m_axis_cmd_tvalid <= 1'b0;
      end

      if (s_axis_tvalid && s_axis_tready && (state == ST_FETCH)) begin
        if (ena_get_addr) begin
          m_axis_cmd_tdata <= address;
          m_axis_cmd_tuser <= CMD_GET_ADR;
          m_axis_cmd_tlast <= 1'b1;
        end else if (ena_error) begin
          m_axis_cmd_tdata <= last_error;
          m_axis_cmd_tuser <= CMD_GET_ERR;
          m_axis_cmd_tlast <= 1'b1;
        end else if (ena_echo) begin
          m_axis_cmd_tdata <= s_axis_tdata;
          m_axis_cmd_tuser <= s_axis_tuser;
          m_axis_cmd_tlast <= s_axis_tlast;
        end
      end
    end else begin
      m_axis_cmd_tvalid <= '0;
      m_axis_cmd_tdata <= '0;
      m_axis_cmd_tuser <= '0;
      m_axis_cmd_tlast <= '0;
    end
  end

endmodule
