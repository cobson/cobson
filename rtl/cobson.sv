/*
  parameter bit [7:0] DEFAULT_ADDR = 8'h00;

  logic        cobson__irst          ;
  logic        cobson__iclk_in       ;
  logic        cobson__iclk_out      ;
  logic        cobson__iclk          ;
  logic        cobson__iuart_rx      ;
  logic        cobson__ouart_tx      ;
  logic [15:0] cobson__idivider      ;
  logic        cobson__s_axis_tvalid ;
  logic  [7:0] cobson__s_axis_tdata  ;
  logic  [7:0] cobson__s_axis_tuser  ;
  logic        cobson__s_axis_tlast  ;
  logic        cobson__s_axis_tready ;
  logic        cobson__m_axis_tvalid ;
  logic  [7:0] cobson__m_axis_tdata  ;
  logic  [7:0] cobson__m_axis_tuser  ;
  logic        cobson__m_axis_tlast  ;
  logic        cobson__m_axis_tready ;

  cobson
  #(
    . DEFAULT_ADDR (DEFAULT_ADDR               ) 
  )
  cobson
  (
    .irst          (cobson__irst               ) ,
    .iclk_in       (cobson__iclk_in            ) ,
    .iclk_out      (cobson__iclk_out           ) ,
    .iclk          (cobson__iclk               ) ,
    .iuart_rx      (cobson__iuart_rx           ) ,
    .ouart_tx      (cobson__ouart_tx           ) ,
    .idivider      (cobson__idivider           ) ,
    .s_axis_tvalid (cobson__s_axis_tvalid      ) ,
    .s_axis_tdata  (cobson__s_axis_tdata       ) ,
    .s_axis_tuser  (cobson__s_axis_tuser       ) ,
    .s_axis_tlast  (cobson__s_axis_tlast       ) ,
    .s_axis_tready (cobson__s_axis_tready      ) ,
    .m_axis_tvalid (cobson__m_axis_tvalid      ) ,
    .m_axis_tdata  (cobson__m_axis_tdata       ) ,
    .m_axis_tuser  (cobson__m_axis_tuser       ) ,
    .m_axis_tlast  (cobson__m_axis_tlast       ) ,
    .m_axis_tready (cobson__m_axis_tready      ) 
  );

  assign cobson__irst          = '0;
  assign cobson__iclk_in       = '0;
  assign cobson__iclk_out      = '0;
  assign cobson__iclk          = '0;
  assign cobson__idivider      = '0;
  assign cobson__iuart_rx      = '0;
  assign cobson__s_axis_tvalid = '0;
  assign cobson__s_axis_tuser  = '0;
  assign cobson__s_axis_tdata  = '0;
  assign cobson__s_axis_tlast  = '0;
  assign cobson__m_axis_tready = '0;
*/

`include "cobson.svh"

module cobson
#(
  parameter bit [7:0] DEFAULT_ADDR = 8'h00
)
(
  input  logic        irst          ,
  input  logic        iclk_in       ,
  input  logic        iclk_out      ,
  input  logic        iclk          ,
  //
  input  logic        iuart_rx      ,
  output logic        ouart_tx      ,
  input  logic [15:0] idivider      ,
  //
  input  logic        s_axis_tvalid ,
  input  logic  [7:0] s_axis_tdata  ,
  input  logic  [7:0] s_axis_tuser  ,
  input  logic        s_axis_tlast  ,
  output logic        s_axis_tready ,
  //
  output logic        m_axis_tvalid ,
  output logic  [7:0] m_axis_tdata  ,
  output logic  [7:0] m_axis_tuser  ,
  output logic        m_axis_tlast  ,
  input  logic        m_axis_tready 
);

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  parameter int DATA_BUF_W = 9;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic                ctrl__oreset;
  stat_t               ctrl__istat;
  logic          [7:0] ctrl__oaddress;
  logic                ctrl__s_axis_tvalid;
  logic          [7:0] ctrl__s_axis_tdata;
  logic          [7:0] ctrl__s_axis_tuser;
  logic                ctrl__s_axis_tlast;
  logic                ctrl__s_axis_tready;
  logic                ctrl__m_axis_tvalid;
  logic          [7:0] ctrl__m_axis_tdata;
  logic          [7:0] ctrl__m_axis_tuser;
  logic                ctrl__m_axis_tlast;
  logic                ctrl__m_axis_tready;
  logic                ctrl__m_axis_cmd_tvalid;
  logic          [7:0] ctrl__m_axis_cmd_tdata;
  logic          [7:0] ctrl__m_axis_cmd_tuser;
  logic                ctrl__m_axis_cmd_tlast;
  logic                ctrl__m_axis_cmd_tready;

  logic                rx__s_axis_tvalid;
  logic          [7:0] rx__s_axis_tdata;
  logic                rx__s_axis_tuser;
  logic                rx__s_axis_tlast;
  logic                rx__s_axis_tready;
  logic                rx__m_axis_tvalid;
  logic          [7:0] rx__m_axis_tdata;
  logic          [7:0] rx__m_axis_tuser;
  logic                rx__m_axis_tlast;
  logic                rx__m_axis_tready;
  rx_stat_t            rx__ostat;
  logic          [7:0] rx__iaddress;

  logic          [7:0] tx__iaddress;
  logic                tx__s_axis_tvalid;
  logic          [7:0] tx__s_axis_tdata;
  logic          [7:0] tx__s_axis_tuser;
  logic                tx__s_axis_tlast;
  logic                tx__s_axis_tready;
  logic                tx__m_axis_tvalid;
  logic          [7:0] tx__m_axis_tdata;
  logic          [7:0] tx__m_axis_tuser;
  logic                tx__m_axis_tlast;
  logic                tx__m_axis_tready;
  logic                tx__s_axis_cmd_tvalid;
  logic          [7:0] tx__s_axis_cmd_tdata;
  logic          [7:0] tx__s_axis_cmd_tuser;
  logic                tx__s_axis_cmd_tlast;
  logic                tx__s_axis_cmd_tready;
  tx_stat_t            tx__ostat;

  logic         [15:0] tr__idivider;
  logic                tr__s_axis_tvalid;
  logic          [7:0] tr__s_axis_tdata;
  logic          [7:0] tr__s_axis_tuser;
  logic                tr__s_axis_tlast;
  logic                tr__s_axis_tready;
  logic                tr__m_axis_tvalid;
  logic          [7:0] tr__m_axis_tdata;
  logic                tr__m_axis_tuser;
  logic                tr__m_axis_tlast;
  logic                tr__m_axis_tready;
  logic                tr__iuart_rx;
  logic                tr__ouart_tx;
  tr_stat_t            tr__ostat;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  assign s_axis_tready = tx__s_axis_tready;

  assign ouart_tx = tr__ouart_tx;

  assign m_axis_tvalid = ctrl__m_axis_tvalid;
  assign m_axis_tdata = ctrl__m_axis_tdata;
  assign m_axis_tuser = ctrl__m_axis_tuser;
  assign m_axis_tlast = ctrl__m_axis_tlast;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  assign tr__idivider            = idivider;
  assign tr__s_axis_tvalid       = tx__m_axis_tvalid;
  assign tr__s_axis_tdata        = tx__m_axis_tdata;
  assign tr__s_axis_tuser        = tx__m_axis_tuser;
  assign tr__s_axis_tlast        = tx__m_axis_tlast;
  assign tr__m_axis_tready       = rx__s_axis_tready;
  assign tr__iuart_rx            = iuart_rx;

  assign ctrl__istat.rx          = rx__ostat;
  assign ctrl__istat.tx          = tx__ostat;
  assign ctrl__istat.tr          = tr__ostat;
  assign ctrl__s_axis_tvalid     = rx__m_axis_tvalid;
  assign ctrl__s_axis_tdata      = rx__m_axis_tdata;
  assign ctrl__s_axis_tuser      = rx__m_axis_tuser;
  assign ctrl__s_axis_tlast      = rx__m_axis_tlast;
  assign ctrl__m_axis_cmd_tready = tx__s_axis_cmd_tready;
  assign ctrl__m_axis_tready     = m_axis_tready;

  assign rx__s_axis_tvalid       = tr__m_axis_tvalid;
  assign rx__s_axis_tdata        = tr__m_axis_tdata;
  assign rx__s_axis_tlast        = tr__m_axis_tlast;
  assign rx__s_axis_tuser        = tr__m_axis_tuser;
  assign rx__m_axis_tready       = ctrl__s_axis_tready;
  assign rx__iaddress            = ctrl__oaddress;

  assign tx__iaddress            = ctrl__oaddress;
  assign tx__s_axis_tvalid       = s_axis_tvalid;
  assign tx__s_axis_tdata        = s_axis_tdata;
  assign tx__s_axis_tlast        = s_axis_tlast;
  assign tx__s_axis_tuser        = s_axis_tuser;
  assign tx__m_axis_tready       = tr__s_axis_tready;
  assign tx__s_axis_cmd_tvalid   = ctrl__m_axis_cmd_tvalid;
  assign tx__s_axis_cmd_tdata    = ctrl__m_axis_cmd_tdata;
  assign tx__s_axis_cmd_tuser    = ctrl__m_axis_cmd_tuser;
  assign tx__s_axis_cmd_tlast    = ctrl__m_axis_cmd_tlast;

  cobson_ctrl
  #(
    . DEFAULT_ADDR     (DEFAULT_ADDR                 ) ,
    . BUF_W            (DATA_BUF_W                   )
  )
  ctrl
  (
    .irst              (irst                         ) ,
    .iclk              (iclk                         ) ,
    .oreset            (ctrl__oreset                 ) ,
    .istat             (ctrl__istat                  ) ,
    .oaddress          (ctrl__oaddress               ) ,
    .s_axis_tvalid     (ctrl__s_axis_tvalid          ) ,
    .s_axis_tdata      (ctrl__s_axis_tdata           ) ,
    .s_axis_tuser      (ctrl__s_axis_tuser           ) ,
    .s_axis_tlast      (ctrl__s_axis_tlast           ) ,
    .s_axis_tready     (ctrl__s_axis_tready          ) ,
    .m_axis_tvalid     (ctrl__m_axis_tvalid          ) ,
    .m_axis_tdata      (ctrl__m_axis_tdata           ) ,
    .m_axis_tuser      (ctrl__m_axis_tuser           ) ,
    .m_axis_tlast      (ctrl__m_axis_tlast           ) ,
    .m_axis_tready     (ctrl__m_axis_tready          ) ,
    .m_axis_cmd_tvalid (ctrl__m_axis_cmd_tvalid      ) ,
    .m_axis_cmd_tdata  (ctrl__m_axis_cmd_tdata       ) ,
    .m_axis_cmd_tuser  (ctrl__m_axis_cmd_tuser       ) ,
    .m_axis_cmd_tlast  (ctrl__m_axis_cmd_tlast       ) ,
    .m_axis_cmd_tready (ctrl__m_axis_cmd_tready      ) 
  );

  cobson_rx
  #(
    . DATA_BUF_W       (DATA_BUF_W                   ) ,
    . RESP_BUF_W       (DATA_BUF_W                   )
  )
  cobson_rx__
  (
    .irst              (irst                         ) ,
    .iclk              (iclk                         ) ,
    .s_axis_tvalid     (rx__s_axis_tvalid            ) ,
    .s_axis_tdata      (rx__s_axis_tdata             ) ,
    .s_axis_tuser      (rx__s_axis_tuser             ) ,
    .s_axis_tlast      (rx__s_axis_tlast             ) ,
    .s_axis_tready     (rx__s_axis_tready            ) ,
    .m_axis_tvalid     (rx__m_axis_tvalid            ) ,
    .m_axis_tdata      (rx__m_axis_tdata             ) ,
    .m_axis_tuser      (rx__m_axis_tuser             ) ,
    .m_axis_tlast      (rx__m_axis_tlast             ) ,
    .m_axis_tready     (rx__m_axis_tready            ) ,
    .iaddress          (rx__iaddress                 ) ,
    .ostat             (rx__ostat                    )
  );

  cobson_tx
  #(
    . DATA_BUF_W       (DATA_BUF_W                   ) 
  )
  cobson_tx
  (
    .irst              (irst                         ) ,
    .iclk_in           (iclk                         ) ,
    .iclk              (iclk                         ) ,
    .iaddress          (tx__iaddress                 ) ,
    .s_axis_tvalid     (tx__s_axis_tvalid            ) ,
    .s_axis_tdata      (tx__s_axis_tdata             ) ,
    .s_axis_tuser      (tx__s_axis_tuser             ) ,
    .s_axis_tlast      (tx__s_axis_tlast             ) ,
    .s_axis_tready     (tx__s_axis_tready            ) ,
    .m_axis_tvalid     (tx__m_axis_tvalid            ) ,
    .m_axis_tdata      (tx__m_axis_tdata             ) ,
    .m_axis_tlast      (tx__m_axis_tlast             ) ,
    .m_axis_tready     (tx__m_axis_tready            ) ,
    .s_axis_cmd_tvalid (tx__s_axis_cmd_tvalid        ) ,
    .s_axis_cmd_tdata  (tx__s_axis_cmd_tdata         ) ,
    .s_axis_cmd_tuser  (tx__s_axis_cmd_tuser         ) ,
    .s_axis_cmd_tlast  (tx__s_axis_cmd_tlast         ) ,
    .s_axis_cmd_tready (tx__s_axis_cmd_tready        ) ,
    .ostat             (tx__ostat                    )
  );

  cobson_transport
  tr
  (
    .irst              (irst                         ) ,
    .iclk              (iclk                         ) ,
    .idivider          (tr__idivider                 ) ,
    .s_axis_tvalid     (tr__s_axis_tvalid            ) ,
    .s_axis_tdata      (tr__s_axis_tdata             ) ,
    .s_axis_tuser      (tr__s_axis_tuser             ) ,
    .s_axis_tlast      (tr__s_axis_tlast             ) ,
    .s_axis_tready     (tr__s_axis_tready            ) ,
    .m_axis_tvalid     (tr__m_axis_tvalid            ) ,
    .m_axis_tdata      (tr__m_axis_tdata             ) ,
    .m_axis_tuser      (tr__m_axis_tuser             ) ,
    .m_axis_tlast      (tr__m_axis_tlast             ) ,
    .m_axis_tready     (tr__m_axis_tready            ) ,
    .iuart_rx          (tr__iuart_rx                 ) ,
    .ouart_tx          (tr__ouart_tx                 ) ,
    .ostat             (tr__ostat                    ) 
  );

endmodule
