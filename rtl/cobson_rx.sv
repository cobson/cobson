/*
  parameter int DATA_BUF_W = 9;

  logic       rx__irst              ;
  logic       rx__iclk              ;
  logic       rx__s_axis_tvalid     ;
  logic [7:0] rx__s_axis_tdata      ;
  logic       rx__s_axis_tuser      ;
  logic       rx__s_axis_tlast      ;
  logic       rx__s_axis_tready     ;
  logic       rx__m_axis_tvalid     ;
  logic [7:0] rx__m_axis_tdata      ;
  logic [7:0] rx__m_axis_tuser      ;
  logic       rx__m_axis_tlast      ;
  logic       rx__m_axis_tready     ;
  rx_stat_t   rx__ostat             ;
  logic [7:0] rx__iaddress          ;

  cobson_rx
  #(
    . DATA_BUF_W       (DATA_BUF_W      )
  )
  cobson_rx__
  (
    .irst              (rx__irst                   ) ,
    .iclk              (rx__iclk                   ) ,
    .s_axis_tvalid     (rx__s_axis_tvalid          ) ,
    .s_axis_tdata      (rx__s_axis_tdata           ) ,
    .s_axis_tuser      (rx__s_axis_tuser           ) ,
    .s_axis_tlast      (rx__s_axis_tlast           ) ,
    .s_axis_tready     (rx__s_axis_tready          ) ,
    .m_axis_tvalid     (rx__m_axis_tvalid          ) ,
    .m_axis_tdata      (rx__m_axis_tdata           ) ,
    .m_axis_tuser      (rx__m_axis_tuser           ) ,
    .m_axis_tlast      (rx__m_axis_tlast           ) ,
    .m_axis_tready     (rx__m_axis_tready          ) ,
    .iaddress          (rx__iaddress               ) ,
    .ostat             (rx__ostat                  )
  );

  assign rx__irst              = '0;
  assign rx__iclk              = '0;
  assign rx__s_axis_tvalid     = '0;
  assign rx__s_axis_tdata      = '0;
  assign rx__s_axis_tlast      = '0;
  assign rx__m_axis_tready     = '0;
  assign rx__iaddress          = '0;
*/ 

`include "cobson.svh"

module cobson_rx
#(
  parameter int DATA_BUF_W = 10
)
(
  input  logic       irst              ,
  input  logic       iclk              ,
  //
  input  logic       s_axis_tvalid     ,
  input  logic [7:0] s_axis_tdata      ,
  input  logic       s_axis_tuser      ,
  input  logic       s_axis_tlast      ,
  output logic       s_axis_tready     ,
  //
  output logic       m_axis_tvalid     ,
  output logic [7:0] m_axis_tdata      ,
  output logic [7:0] m_axis_tuser      ,
  output logic       m_axis_tlast      ,
  input  logic       m_axis_tready     ,
  //
  input  logic [7:0] iaddress          ,
  output rx_stat_t   ostat
);
  
  localparam int DATA_WORDS = 2**DATA_BUF_W;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  logic                           db__s_axis_tready;
  logic                     [7:0] db__m_axis_tdata;
  logic                           db__m_axis_tready;
  logic                           db__m_axis_tvalid;
  logic                           db__m_axis_tuser;
  logic                           db__m_axis_tlast;
  logic  [$clog2(DATA_WORDS)-1:0] db__used;
  logic                           db__empty;

  logic           fifo_tready;
  logic     [7:0] cnt_dat;
  logic     [7:0] size_dat;
  logic     [7:0] crc;
  logic     [7:0] last_sample;
  logic           data_val;
  logic           data_eop;
  
  enum logic [3:0] {
    ST_IDLE       = 4'd0,
    ST_GET_ADDR   = 4'd1,
    ST_CHECK_ADDR = 4'd2,
    ST_FLUSH      = 4'd3,
    ST_GET_CMD    = 4'd4,
    ST_CHECK_CMD  = 4'd5,
    ST_GET_SIZE   = 4'd6,
    ST_CHECK_SIZE = 4'd7,
    ST_GET_DATA   = 4'd8,
    ST_GET_CRC    = 4'd9,
    ST_CHECK_CRC  = 4'd10,
    ST_END        = 4'd11
  } state;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Data buffer
  always_comb begin
    if (state == ST_GET_DATA)
      db__m_axis_tready = (fifo_tready && m_axis_tready);
    else
      db__m_axis_tready = fifo_tready;
  end

  axis_sfifo
  #(
    . DATA_WIDTH    (8                  ) ,
    . DEPTH         (DATA_WORDS         ) ,
    . FRAME         (1                  ) ,
    . USER_ENA      (1                  ) ,
    . USER_WIDTH    (1                  ) ,
    . LAST_ENA      (1                  )
  )
  data_buf
  (
    .iclk           (iclk               ) ,
    .irst           (irst               ) ,
    .s_axis_tdata   (s_axis_tdata       ) ,
    .s_axis_tvalid  (s_axis_tvalid      ) ,
    .s_axis_tlast   (s_axis_tlast       ) ,
    .s_axis_tuser   (s_axis_tuser       ) ,
    .s_axis_tready  (db__s_axis_tready  ) ,
    .m_axis_tdata   (db__m_axis_tdata   ) ,
    .m_axis_tready  (db__m_axis_tready  ) ,
    .m_axis_tvalid  (db__m_axis_tvalid  ) ,
    .m_axis_tlast   (db__m_axis_tlast   ) ,
    .m_axis_tuser   (db__m_axis_tuser   ) ,
    .used           (db__used           ) ,
    .empty          (db__empty          ) ,
    .full           () ,
    .overflow       () ,
    .underflow      () 
  );

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Tricky readout FSM
  assign data_val = db__m_axis_tvalid && db__m_axis_tready;
  assign data_eop = (cnt_dat == size_dat);

  always_ff @(posedge iclk) begin
    if (irst)  state <= ST_IDLE;
    else case (state)
      // Wait for at least one frame in the FIFO
      ST_IDLE         : begin
        if (~db__empty)
          state <= ST_GET_ADDR;
      end
      // Get device's address
      ST_GET_ADDR     : begin
        if (db__m_axis_tvalid)
          state <= ST_CHECK_ADDR;
      end
      // If address is correct - go next, otherwise flush current packet
      ST_CHECK_ADDR   : begin
        if (iaddress == db__m_axis_tdata)
          state <= ST_GET_CMD;
        else
          state <= ST_FLUSH;
      end
      // Just flush current packet (! not FIFO)
      ST_FLUSH        : begin
        if (data_val && db__m_axis_tlast)
          state <= ST_IDLE;
        else if (db__empty)
          state <= ST_IDLE;
      end
      // Get command
      ST_GET_CMD      : begin
        if (db__m_axis_tvalid)
          state <= ST_CHECK_CMD;
      end
      //
      ST_CHECK_CMD    : begin
        state <= ST_GET_SIZE;
      end
      // Data size
      ST_GET_SIZE     : begin
        if (db__m_axis_tvalid)
          state <= ST_CHECK_SIZE;
      end
      // Check size for incorrect values. Not sure that it's necessary...
      ST_CHECK_SIZE   : begin
        if (db__m_axis_tdata > 8'h00)
          state <= ST_GET_DATA;
        else
          state <= ST_FLUSH;
      end
      // Get data pack
      ST_GET_DATA     : begin
        if (data_val && db__m_axis_tlast)
          state <= ST_FLUSH; // not full packet
        else if (data_val && data_eop)
          state <= ST_GET_CRC;
      end
      // Get last word - CRC
      ST_GET_CRC      : begin
        if (data_val && db__m_axis_tlast)
          state <= ST_CHECK_CRC;
        else if (data_val)
          state <= ST_FLUSH; // incorrect packet - CRC not last byte
      end
      // Check CRC, obviously
      ST_CHECK_CRC    : begin
        if (crc == 8'h00)
          state <= ST_END;
        else
          state <= ST_FLUSH;
      end
      // Packet has:
      // correct address
      // correct size
      // valid CRC
      ST_END          : begin
        state <= ST_IDLE;
      end

      default : state <= ST_IDLE;
    endcase
  end

  always_ff @(posedge iclk) begin
    // Save last data sample and sent it after CRC check
    if (irst)
      last_sample <= '0;
    else if (state == ST_GET_DATA)
      last_sample <= db__m_axis_tdata;

    // Save size of current data packet
    if (irst)
      size_dat <= '0;
    else if (state == ST_CHECK_SIZE)
      size_dat <= db__m_axis_tdata-1'b1;

    // Count of input words
    if (irst)
      cnt_dat <= '0;
    else if (state == ST_GET_SIZE)
      cnt_dat <= '0;
    else if ((state == ST_GET_DATA) && data_val)
      cnt_dat <= cnt_dat + 1'b1;

    if (irst)
      crc <= '0;
    else if (state == ST_IDLE)
      crc <= '0;
    else if (data_val)
      crc <= crc8(db__m_axis_tdata, crc);

    if (irst)
      fifo_tready <= 1'b0;
    else
      fifo_tready <=    (state == ST_FLUSH)
                     || (state == ST_GET_ADDR)
                     || (state == ST_GET_CMD)
                     || (state == ST_GET_SIZE)
                     || (state == ST_GET_DATA);
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------


  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  // Output interface
  always_ff @(posedge iclk) begin
    if (irst)
      m_axis_tvalid <= 1'b0;
    else if (data_val && (state == ST_GET_DATA))
      m_axis_tvalid <= (db__m_axis_tvalid && ~data_eop);
    else if (state == ST_END)
      m_axis_tvalid <= 1'b1; // set only if CRC is valid
    else if (m_axis_tready)
      m_axis_tvalid <= 1'b0;

    if (irst)
      m_axis_tdata <= '0;
    else if (data_val && (state == ST_GET_DATA))
      m_axis_tdata <= db__m_axis_tdata;
    else if (state == ST_END)
      m_axis_tdata <= last_sample; // set only if CRC is valid

    if (irst)
      m_axis_tuser <= '0;
    else if (state == ST_CHECK_CMD)
      m_axis_tuser <= db__m_axis_tdata;

    if (state == ST_END)
      m_axis_tlast <= 1'b1;
    else if (m_axis_tready)
      m_axis_tlast <= 1'b0;
  end

  assign s_axis_tready = db__s_axis_tready;

endmodule
