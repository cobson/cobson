
module top
  (
    input            SYS_CLK_I    ,
    input            USB_UART_RXD ,
    output           USB_UART_TXD ,
    output           USER_LED1    ,
    output           USER_LED2    ,
    output           USER_LED3    ,
    output           USER_LED4    ,
    output           USER_LED5    ,
    output           USER_LED6    ,
    output           USER_LED7    ,
    output           USER_LED8    ,
    input            PB1          ,
    input            PB2          ,
    input            PB3          ,
    input            PB4
  );

  parameter int pSIZE_W = 8;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  logic        clk__080              ;
  logic        clk__locked           ;

  logic        cobson__irst          ;
  logic        cobson__iclk_in       ;
  logic        cobson__iclk_out      ;
  logic        cobson__iclk          ;
  logic        cobson__iuart_rx      ;
  logic        cobson__ouart_tx      ;
  logic [15:0] cobson__idivider      ;
  logic        cobson__s_axis_tvalid ;
  logic  [7:0] cobson__s_axis_tdata  ;
  logic  [7:0] cobson__s_axis_tuser  ;
  logic        cobson__s_axis_tlast  ;
  logic        cobson__s_axis_tready ;
  logic        cobson__m_axis_tvalid ;
  logic  [7:0] cobson__m_axis_tdata  ;
  logic  [7:0] cobson__m_axis_tuser  ;
  logic        cobson__m_axis_tlast  ;
  logic        cobson__m_axis_tready ;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  logic     [27:0] heartbit;
  logic            pll_rst = 1'b1;
  logic            global_reset;
  (* MARK_DEBUG="true" *)logic            tvalid;
  (* MARK_DEBUG="true" *)logic      [7:0] tdata;
  (* MARK_DEBUG="true" *)logic      [7:0] tuser;
  (* MARK_DEBUG="true" *)logic            tready;
  (* MARK_DEBUG="true" *)logic            tlast;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------

  always_ff@(posedge SYS_CLK_I) begin
    pll_rst <= 1'b0;
  end

  always_ff@(posedge clk__080) begin
    heartbit <= heartbit + 1'b1;
  end

  always_ff@(posedge clk__080) begin
    global_reset <= ~clk__locked;
  end

  always_ff@(posedge clk__080) begin
    tready <= 1'b1;

    if (cobson__m_axis_tready && cobson__m_axis_tvalid) begin
      tdata <= cobson__m_axis_tdata;
      tuser <= cobson__m_axis_tuser;
      tlast <= cobson__m_axis_tlast;
    end

    tvalid <= (cobson__m_axis_tready && cobson__m_axis_tvalid);
  end

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  assign USER_LED1    = heartbit[$high(heartbit)];
  assign USER_LED2    = clk__locked;
  assign USER_LED3    = USB_UART_RXD;
  assign USER_LED4    = USB_UART_TXD;

  assign USB_UART_TXD = cobson__ouart_tx;

  //------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------
  
  assign cobson__irst          = global_reset;
  assign cobson__iclk_in       = clk__080;
  assign cobson__iclk_out      = clk__080;
  assign cobson__iclk          = clk__080;
  assign cobson__idivider      = 'd80;
  assign cobson__iuart_rx      = USB_UART_RXD;
  assign cobson__s_axis_tvalid = tvalid;
  assign cobson__s_axis_tuser  = tuser;
  assign cobson__s_axis_tdata  = tdata;
  assign cobson__s_axis_tlast  = tlast;
  assign cobson__m_axis_tready = tready;
  
  clk_wiz_0
  clk
  (
    .clk_out1   (clk__080     ) ,
    .reset      (pll_rst      ) ,
    .locked     (clk__locked  ) ,
    .clk_in1    (SYS_CLK_I    )
  );

  (* keep_hierarchy="yes" *)(* dont_touch="true" *)cobson
  #(
    . pSIZE_W    (pSIZE_W      ) 
  )
  cobson
  (
    .irst          (cobson__irst               ) ,
    .iclk_in       (cobson__iclk_in            ) ,
    .iclk_out      (cobson__iclk_out           ) ,
    .iclk          (cobson__iclk               ) ,
    .iuart_rx      (cobson__iuart_rx           ) ,
    .ouart_tx      (cobson__ouart_tx           ) ,
    .idivider      (cobson__idivider           ) ,
    .s_axis_tvalid (cobson__s_axis_tvalid      ) ,
    .s_axis_tdata  (cobson__s_axis_tdata       ) ,
    .s_axis_tuser  (cobson__s_axis_tuser       ) ,
    .s_axis_tlast  (cobson__s_axis_tlast       ) ,
    .s_axis_tready (cobson__s_axis_tready      ) ,
    .m_axis_tvalid (cobson__m_axis_tvalid      ) ,
    .m_axis_tdata  (cobson__m_axis_tdata       ) ,
    .m_axis_tuser  (cobson__m_axis_tuser       ) ,
    .m_axis_tlast  (cobson__m_axis_tlast       ) ,
    .m_axis_tready (cobson__m_axis_tready      ) 
  );

endmodule
